#include <stdio.h>
#define buff 1024 //we want to leave room for ending "1", so real buff size is 1024-1

void getHash(char str[], int* A, int* B, int* C, int* D);

int  main(int argc, char *argv[]){
    char str[buff];
    /* pre-algorithm values */
    int A = 0x523F0EC4, B = 0xB0964BE0;
    int C = 0x4DE4170F, D = 0x28553231;
    /* pointers to values */
    int* pA = &A;
    int* pB = &B;
    int* pC = &C;
    int* pD = &D; 
    /* now it's time to handle input from console or file */
    /* if there's no args while launch, data to hash will be read from console */
    if (argc<2){
    printf("Enter a string:\n");
    fgets(str,buff,stdin);
    getHash(str,pA,pB,pC,pD);
    }
    /* if there's more than one arg, we'll ask to pass files one by one */
    else if (argc > 2){
        printf("Please, pass only one file at time as argument.\n");
        return 1;
    }
    /* if there's only one arg, we'll try to get hash of the file with name passed as that arg */
    else{
        FILE* in;
        in = fopen(argv[1],"rb");
        if (!in){
            printf("Error while reading file.\n");
            return 1;
        }
        else{
            while (!feof(in)){
                int n = fread(str,sizeof(char),buff-1,in);
                str[n]='\0'; //we need to mark end of useful data with \0 for hash searching function
                getHash(str,pA,pB,pC,pD);
            }
            fclose(in);
        }
    }
    /* three formated integers to output 12 bytes of data as a final hash sum */
    int DC = (D << 11) + (C>>20);
    int CB = (C << 11) + (B >> 14);
    int BA = (B << 16) + A;
    /* formatted output of hash to console */
    printf("%08x%08x%08x\n",DC,CB,BA);

    return 0;
}

void getHash(char str[], int* A, int* B, int* C, int* D){
    /* input stream size counter */
    const char* ch = str;
    int size=0;
    while (*(ch++)){
        size++;
    }
    if (str[size-1]=='\n'){ //to get rid of additional newline symbol at the end when line entered via console
        str[--size]='\0';
    }
    /* as every char symbol in c has size 1 byte, we can work with ints w/o converting them to binaries */
    int endingZeros = 16 - ((size+1) % 16); // as we're going to take 4 bytes for each letter in out hash we need total 16 bytes of data for each iteration
    int modBuff = size+1+endingZeros;
    char modStr[modBuff];
    for (int i = 0; i < modBuff; i++){
        if(i < size)
            modStr[i]=str[i];
        else if (i == size)
            modStr[i]=1;
        else
            modStr[i]=0;
    }
    /* now we're going to modify hash value by operating with incoming bytes */
    for (int i = 0; i < modBuff; i+=4){
    /* math operations to get unique (kind of) hash */
        *A = (((*A)<<5 + modStr[i]*17)^(*C))*(modStr[i]*modStr[i+1]) + ((*D)>>2);
        *B = ((*B*3 + modStr[i+1])^modStr[i])*(*A^*D);
        *C = (*C<<3)^(modStr[i+2]+modStr[i])*((*A)^(*B));
        *D = ((((((*D)*modStr[i] <<6)^modStr[i] + 5) * (*B)) << 3) + modStr[i]) - modStr[i+3];
    }
    return;
}
