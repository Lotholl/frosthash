# frosthash
## Simple hash function with custom hash algorythm. 
### Usage:
To get hash of file just pass it's name as an argument to program. The program can't handle more than one file at the time.
If you need a hash of a string, just launch program in console with no arguments, then enter your string when asked.
